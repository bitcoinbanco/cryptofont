const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')

// Set output paths
buildPath = path.join(__dirname, 'dist')

module.exports = {
  entry: {
    cryptofont: './cryptofont.js',
    'docs/docs': './docs/index.js'
  },

  output: {
    path: buildPath,
    filename: '[name].js'
  },

  // optimization: {
  //   splitChunks: {
  //     chunks: 'all'
  //   }
  // },

  module: {
    rules: [
      // Compile SCSS files
      {
        test: /\.scss$/,
        // This compiles styles specific to this app
        // include: path.join(__dirname, 'scss'),
        use: [
          { loader: MiniCssExtractPlugin.loader },
          {
            loader: 'css-loader',
            options: { sourceMap: true }
          },
          { loader: 'resolve-url-loader' },
          {
            loader: 'postcss-loader',
            options: {
              plugins: () => [ require('precss'), require('autoprefixer') ]
            }
          },
          {
            loader: 'sass-loader',
            options: { sourceMap: true }
          },
        ],
      },

      // Compile fonts
      {
        test: /\.(woff|woff2|eot|ttf|svg)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]',
          outputPath: 'fonts/'
        }
      }
    ]
  },

  plugins: [
    new CleanWebpackPlugin(),

		new MiniCssExtractPlugin({
      filename: '[name].css'
    }),

    new HtmlWebpackPlugin({
      filename: path.join(buildPath, 'docs/index.html'),
      template: 'docs/index.html'
      // filename: path.join(buildPath, `${view}.html`),
      // template: path.join(sourcePath, `${view}.pug`),
      // chunks: ['app', `js/${view}`]
    })
  ]
}
