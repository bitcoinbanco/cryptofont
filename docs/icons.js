export default [
  "cf-0xbtc",
  "cf-1wo",
  "cf-8bit",
  "cf-42",
  "cf-888",
  "cf-1337",
  "cf-abt",
  "cf-aby",
  "cf-abyss",
  "cf-ac3",
  "cf-acat",
  "cf-acc",
  "cf-ace",
  "cf-act",
  "cf-ada",
  "cf-adb",
  "cf-adc",
  "cf-adh",
  "cf-adi",
  "cf-adst",
  "cf-adt",
  "cf-adx",
  "cf-ae",
  "cf-aeon",
  "cf-aerm",
  "cf-agi",
  "cf-aht",
  "cf-ai",
  "cf-aib",
  "cf-aid",
  "cf-aidoc",
  "cf-aion",
  "cf-air",
  "cf-ait",
  "cf-aix",
  "cf-alis",
  "cf-alqo",
  "cf-alt",
  "cf-amb",
  "cf-amlt",
  "cf-amm",
  "cf-amn",
  "cf-amp",
  "cf-ams",
  "cf-anc",
  "cf-ant",
  "cf-aph",
  "cf-apis",
  "cf-appc",
  "cf-apr",
  "cf-arc",
  "cf-arct",
  "cf-ardr",
  "cf-arg",
  "cf-ark",
  "cf-arn",
  "cf-art",
  "cf-ary",
  "cf-ast",
  "cf-astro",
  "cf-atb",
  "cf-atc",
  "cf-atl",
  "cf-atm",
  "cf-atmc",
  "cf-atn",
  "cf-atom",
  "cf-ats",
  "cf-atx",
  "cf-auc",
  "cf-aur",
  "cf-aura",
  "cf-avt",
  "cf-axp",
  "cf-b2b",
  "cf-banca",
  "cf-bank",
  "cf-bat",
  "cf-bax",
  "cf-bay",
  "cf-bbi",
  "cf-bbn",
  "cf-bbo",
  "cf-bbp",
  "cf-bbr",
  "cf-bcc",
  "cf-bcd",
  "cf-bcn",
  "cf-bco",
  "cf-bcpt",
  "cf-bdg",
  "cf-bdl",
  "cf-bee",
  "cf-bela",
  "cf-berry",
  "cf-bet",
  "cf-betr",
  "cf-bez",
  "cf-bft",
  "cf-bigup",
  "cf-birds",
  "cf-bis",
  "cf-bitb",
  "cf-bitg",
  "cf-bits",
  "cf-bitz",
  "cf-bix",
  "cf-bkx",
  "cf-blc",
  "cf-blitz",
  "cf-blk",
  "cf-bln",
  "cf-block",
  "cf-blt",
  "cf-blu",
  "cf-blue",
  "cf-blz",
  "cf-blz2",
  "cf-bmc",
  "cf-bmh",
  "cf-bnb",
  "cf-bnk",
  "cf-bnt",
  "cf-bnty",
  "cf-boat",
  "cf-bon",
  "cf-bos",
  "cf-bot",
  "cf-bpt",
  "cf-bqx",
  "cf-brat",
  "cf-brd",
  "cf-bsd",
  "cf-bsm",
  "cf-bta",
  "cf-btc",
  "cf-btca",
  "cf-btcd",
  "cf-btcp",
  "cf-btdx",
  "cf-bte",
  "cf-btg",
  "cf-btm",
  "cf-btm2",
  "cf-bto",
  "cf-btrn",
  "cf-bts",
  "cf-btx",
  "cf-burst",
  "cf-buzz",
  "cf-bwk",
  "cf-byc",
  "cf-c20",
  "cf-can",
  "cf-candy",
  "cf-cann",
  "cf-capp",
  "cf-carbon",
  "cf-cas",
  "cf-cat",
  "cf-cat2",
  "cf-caz",
  "cf-cbc",
  "cf-cbt",
  "cf-cbx",
  "cf-ccrb",
  "cf-cdn",
  "cf-cdt",
  "cf-ceek",
  "cf-cennz",
  "cf-cfi",
  "cf-chc",
  "cf-chp",
  "cf-chsb",
  "cf-chx",
  "cf-cjt",
  "cf-cl",
  "cf-clam",
  "cf-cln",
  "cf-clo",
  "cf-cloak",
  "cf-clr",
  "cf-cmpco",
  "cf-cms",
  "cf-cmt",
  "cf-cnd",
  "cf-cnet",
  "cf-cnn",
  "cf-cnx",
  "cf-coal",
  "cf-cob",
  "cf-colx",
  "cf-con",
  "cf-coss",
  "cf-cov",
  "cf-coval",
  "cf-cpc",
  "cf-cpc2",
  "cf-cpn",
  "cf-cpx",
  "cf-cpy",
  "cf-crave",
  "cf-crb",
  "cf-crc",
  "cf-cre",
  "cf-crea",
  "cf-cred",
  "cf-credo",
  "cf-crm",
  "cf-crop",
  "cf-crpt",
  "cf-crw",
  "cf-cs",
  "cf-csc",
  "cf-csno",
  "cf-ctr",
  "cf-ctxc",
  "cf-cure",
  "cf-cv",
  "cf-cvc",
  "cf-cvcoin",
  "cf-cvt",
  "cf-cxo",
  "cf-dadi",
  "cf-dai",
  "cf-dan",
  "cf-dasc",
  "cf-dash",
  "cf-dat",
  "cf-data",
  "cf-datx",
  "cf-dax",
  "cf-daxx",
  "cf-day",
  "cf-dbc",
  "cf-dbet",
  "cf-dcn",
  "cf-dcr",
  "cf-dct",
  "cf-dcy",
  "cf-ddd",
  "cf-ddf",
  "cf-deb",
  "cf-dent",
  "cf-dero",
  "cf-deus",
  "cf-dev",
  "cf-dew",
  "cf-dft",
  "cf-dgb",
  "cf-dgd",
  "cf-dgpt",
  "cf-dgtx",
  "cf-dice",
  "cf-dim",
  "cf-dime",
  "cf-divx",
  "cf-dix",
  "cf-dlisk",
  "cf-dlt",
  "cf-dmb",
  "cf-dmd",
  "cf-dml",
  "cf-dmt",
  "cf-dna",
  "cf-dnr",
  "cf-dnt",
  "cf-dock",
  "cf-doge",
  "cf-dp",
  "cf-dpy",
  "cf-drgn",
  "cf-drop",
  "cf-drpu",
  "cf-drt",
  "cf-drxne",
  "cf-dsh",
  "cf-dsr",
  "cf-dta",
  "cf-dtb",
  "cf-dth",
  "cf-dtr",
  "cf-dtrc",
  "cf-duo",
  "cf-dxt",
  "cf-earth",
  "cf-ebtc",
  "cf-eca",
  "cf-ecash",
  "cf-ecc",
  "cf-ecob",
  "cf-edg",
  "cf-edo",
  "cf-edr",
  "cf-edt",
  "cf-edu",
  "cf-efl",
  "cf-efx",
  "cf-efyt",
  "cf-egc",
  "cf-egcc",
  "cf-eko",
  "cf-ekt",
  "cf-el",
  "cf-ela",
  "cf-elec",
  "cf-elf",
  "cf-elix",
  "cf-ella",
  "cf-eltcoin",
  "cf-emb",
  "cf-emc",
  "cf-emc2",
  "cf-eng",
  "cf-enj",
  "cf-enrg",
  "cf-ent",
  "cf-eos",
  "cf-eosdac",
  "cf-epc",
  "cf-eql",
  "cf-eqt",
  "cf-erc",
  "cf-erc20",
  "cf-ero",
  "cf-esp",
  "cf-esz",
  "cf-etbs",
  "cf-etc",
  "cf-eth",
  "cf-etn",
  "cf-etp",
  "cf-ett",
  "cf-etz",
  "cf-euc",
  "cf-evc",
  "cf-eve",
  "cf-evn",
  "cf-evr",
  "cf-evx",
  "cf-excl",
  "cf-exy",
  "cf-face",
  "cf-fair",
  "cf-fct",
  "cf-fdx",
  "cf-fdz",
  "cf-fil",
  "cf-fjc",
  "cf-flash",
  "cf-flixx",
  "cf-flo",
  "cf-fluz",
  "cf-fnd",
  "cf-for",
  "cf-fota",
  "cf-frec",
  "cf-frst",
  "cf-fsn",
  "cf-fst",
  "cf-ft",
  "cf-ftc",
  "cf-ftx",
  "cf-fuel",
  "cf-fun",
  "cf-func",
  "cf-fxt",
  "cf-fyn",
  "cf-game",
  "cf-gat",
  "cf-gb",
  "cf-gbx",
  "cf-gbyte",
  "cf-gcc",
  "cf-gcs",
  "cf-gen",
  "cf-gene",
  "cf-get",
  "cf-getx",
  "cf-gin",
  "cf-gla",
  "cf-gno",
  "cf-gnt",
  "cf-gnx",
  "cf-go",
  "cf-god",
  "cf-golf",
  "cf-golos",
  "cf-good",
  "cf-grc",
  "cf-grft",
  "cf-grid",
  "cf-grlc",
  "cf-grmd",
  "cf-grn",
  "cf-grs",
  "cf-gsc",
  "cf-gtc",
  "cf-gto",
  "cf-guess",
  "cf-gup",
  "cf-gvt",
  "cf-gxs",
  "cf-hac",
  "cf-hade",
  "cf-hat",
  "cf-hav",
  "cf-hbc",
  "cf-heat",
  "cf-her",
  "cf-hero",
  "cf-hgt",
  "cf-hire",
  "cf-hkn",
  "cf-hlc",
  "cf-hmc",
  "cf-hmq",
  "cf-hot",
  "cf-hot2",
  "cf-hpb",
  "cf-hpc",
  "cf-hqx",
  "cf-hsr",
  "cf-hst",
  "cf-ht",
  "cf-huc",
  "cf-hur",
  "cf-hush",
  "cf-hvn",
  "cf-hxx",
  "cf-hydro",
  "cf-ic",
  "cf-icn",
  "cf-icon",
  "cf-icx",
  "cf-idh",
  "cf-idt",
  "cf-idxm",
  "cf-ieth",
  "cf-ift",
  "cf-ignis",
  "cf-iht",
  "cf-iic",
  "cf-inc",
  "cf-incnt",
  "cf-ind",
  "cf-infx",
  "cf-ing",
  "cf-ink",
  "cf-inn",
  "cf-inpay",
  "cf-ins",
  "cf-insn",
  "cf-instar",
  "cf-insur",
  "cf-int",
  "cf-inv",
  "cf-inxt",
  "cf-ioc",
  "cf-ion",
  "cf-iop",
  "cf-iost",
  "cf-miota",
  "cf-iotx",
  "cf-ipl",
  "cf-iqt",
  "cf-isl",
  "cf-itc",
  "cf-itns",
  "cf-itt",
  "cf-ivy",
  "cf-ixc",
  "cf-ixt",
  "cf-j8t",
  "cf-jc",
  "cf-jet",
  "cf-jew",
  "cf-jnt",
  "cf-karma",
  "cf-kb3",
  "cf-kbr",
  "cf-kcs",
  "cf-key",
  "cf-key2",
  "cf-kick",
  "cf-kin",
  "cf-kln",
  "cf-kmd",
  "cf-knc",
  "cf-kobo",
  "cf-kore",
  "cf-krb",
  "cf-krm",
  "cf-krone",
  "cf-kurt",
  "cf-la",
  "cf-lala",
  "cf-latx",
  "cf-lba",
  "cf-lbc",
  "cf-lbtc",
  "cf-lcc",
  "cf-ldc",
  "cf-lend",
  "cf-leo",
  "cf-let",
  "cf-lev",
  "cf-lgd",
  "cf-lgo",
  "cf-linda",
  "cf-link",
  "cf-linx",
  "cf-live",
  "cf-lkk",
  "cf-lmc",
  "cf-lnc",
  "cf-lnd",
  "cf-loc",
  "cf-loci",
  "cf-log",
  "cf-loki",
  "cf-loom",
  "cf-lrc",
  "cf-lsk",
  "cf-ltc",
  "cf-lun",
  "cf-lux",
  "cf-lwf",
  "cf-lyl",
  "cf-lym",
  "cf-mag",
  "cf-maid",
  "cf-man",
  "cf-mana",
  "cf-max",
  "cf-mbrs",
  "cf-mcap",
  "cf-mco",
  "cf-mda",
  "cf-mdc",
  "cf-mds",
  "cf-mdt",
  "cf-mec",
  "cf-med",
  "cf-medic",
  "cf-meet",
  "cf-mer",
  "cf-mfg",
  "cf-mgo",
  "cf-minex",
  "cf-mint",
  "cf-mith",
  "cf-mitx",
  "cf-mkr",
  "cf-mln",
  "cf-mne",
  "cf-mntp",
  "cf-mnx",
  "cf-mnx2",
  "cf-moac",
  "cf-mobi",
  "cf-mod",
  "cf-mof",
  "cf-moin",
  "cf-mojo",
  "cf-mona",
  "cf-moon",
  "cf-morph",
  "cf-mot",
  "cf-mrk",
  "cf-mrq",
  "cf-mscn",
  "cf-msp",
  "cf-msr",
  "cf-mtc",
  "cf-mth",
  "cf-mtl",
  "cf-mtn",
  "cf-mtx",
  "cf-mue",
  "cf-muse",
  "cf-music",
  "cf-mvc",
  "cf-mwat",
  "cf-myb",
  "cf-myst",
  "cf-nanj",
  "cf-nano",
  "cf-nanox",
  "cf-nas",
  "cf-nav",
  "cf-navi",
  "cf-nbai",
  "cf-ncash",
  "cf-nct",
  "cf-nebl",
  "cf-neo",
  "cf-neos",
  "cf-net",
  "cf-neu",
  "cf-newb",
  "cf-nexo",
  "cf-ngc",
  "cf-nio",
  "cf-nkn",
  "cf-nlc2",
  "cf-nlg",
  "cf-nlx",
  "cf-nmc",
  "cf-nmr",
  "cf-nms",
  "cf-nobl",
  "cf-nper",
  "cf-npxs",
  "cf-nsr",
  "cf-ntk",
  "cf-ntrn",
  "cf-nuko",
  "cf-nuls",
  "cf-nvst",
  "cf-nxs",
  "cf-nxt",
  "cf-nyan",
  "cf-oax",
  "cf-obits",
  "cf-oc",
  "cf-occ",
  "cf-ocn",
  "cf-oct",
  "cf-ode",
  "cf-odn",
  "cf-ok",
  "cf-omg",
  "cf-omni",
  "cf-omx",
  "cf-onion",
  "cf-ont",
  "cf-onx",
  "cf-oot",
  "cf-opc",
  "cf-open",
  "cf-opt",
  "cf-ore",
  "cf-ori",
  "cf-orme",
  "cf-ost",
  "cf-otn",
  "cf-otx",
  "cf-oxy",
  "cf-pac",
  "cf-pai",
  "cf-pak",
  "cf-pal",
  "cf-pareto",
  "cf-part",
  "cf-pasc",
  "cf-pasl",
  "cf-pat",
  "cf-pay",
  "cf-payx",
  "cf-pbl",
  "cf-pbt",
  "cf-pcl",
  "cf-pcn",
  "cf-pfr",
  "cf-pho",
  "cf-phr",
  "cf-phs",
  "cf-ping",
  "cf-pink",
  "cf-pirl",
  "cf-pivx",
  "cf-pix",
  "cf-pkc",
  "cf-pkt",
  "cf-plan",
  "cf-play",
  "cf-plbt",
  "cf-plc",
  "cf-plr",
  "cf-plu",
  "cf-plx",
  "cf-pmnt",
  "cf-pnt",
  "cf-poa",
  "cf-poe",
  "cf-polis",
  "cf-poll",
  "cf-poly",
  "cf-pos",
  "cf-post",
  "cf-posw",
  "cf-pot",
  "cf-powr",
  "cf-ppc",
  "cf-ppp",
  "cf-ppt",
  "cf-ppy",
  "cf-pra",
  "cf-pre",
  "cf-prg",
  "cf-prl",
  "cf-pro",
  "cf-proc",
  "cf-prs",
  "cf-pst",
  "cf-ptc",
  "cf-ptoy",
  "cf-pura",
  "cf-pure",
  "cf-pwr",
  "cf-pylnt",
  "cf-pzm",
  "cf-qash",
  "cf-qau",
  "cf-qbic",
  "cf-qbt",
  "cf-qkc",
  "cf-qlc",
  "cf-qlr",
  "cf-qora",
  "cf-qrk",
  "cf-qsp",
  "cf-qtum",
  "cf-qun",
  "cf-qvt",
  "cf-qwark",
  "cf-r",
  "cf-rads",
  "cf-rain",
  "cf-rbt",
  "cf-rby",
  "cf-rcn",
  "cf-rdd",
  "cf-rdn",
  "cf-real",
  "cf-rebl",
  "cf-red",
  "cf-ree",
  "cf-ref",
  "cf-rem",
  "cf-ren",
  "cf-rep",
  "cf-repo",
  "cf-req",
  "cf-rex",
  "cf-rfr",
  "cf-rhoc",
  "cf-ric",
  "cf-rise",
  "cf-rkt",
  "cf-rlc",
  "cf-rmt",
  "cf-rnt",
  "cf-rntb",
  "cf-rpx",
  "cf-ruff",
  "cf-rup",
  "cf-rvn",
  "cf-rvt",
  "cf-safex",
  "cf-saga",
  "cf-sal",
  "cf-salt",
  "cf-san",
  "cf-sbtc",
  "cf-scl",
  "cf-scs",
  "cf-sct",
  "cf-sdc",
  "cf-sdrn",
  "cf-seele",
  "cf-sen",
  "cf-senc",
  "cf-send",
  "cf-sense",
  "cf-sent",
  "cf-seq",
  "cf-seth",
  "cf-sexc",
  "cf-sfc",
  "cf-sgcc",
  "cf-sgn",
  "cf-sgr",
  "cf-shift",
  "cf-ship",
  "cf-shl",
  "cf-shorty",
  "cf-shp",
  "cf-sia",
  "cf-sib",
  "cf-sigt",
  "cf-skb",
  "cf-skin",
  "cf-skm",
  "cf-sky",
  "cf-slg",
  "cf-slr",
  "cf-sls",
  "cf-slt",
  "cf-smart",
  "cf-smc",
  "cf-sms",
  "cf-smt",
  "cf-snc",
  "cf-sngls",
  "cf-snm",
  "cf-sntr",
  "cf-soar",
  "cf-soc",
  "cf-soil",
  "cf-soul",
  "cf-spank",
  "cf-spd",
  "cf-spd2",
  "cf-spf",
  "cf-sphtx",
  "cf-spk",
  "cf-spr",
  "cf-sprts",
  "cf-src",
  "cf-srcoin",
  "cf-srn",
  "cf-ss",
  "cf-ssp",
  "cf-sss",
  "cf-sta",
  "cf-stac",
  "cf-stak",
  "cf-start",
  "cf-stc",
  "cf-steem",
  "cf-stn",
  "cf-stn2",
  "cf-storj",
  "cf-storm",
  "cf-stq",
  "cf-strat",
  "cf-strc",
  "cf-stv",
  "cf-stx",
  "cf-sub",
  "cf-sumo",
  "cf-super",
  "cf-sur",
  "cf-svh",
  "cf-swftc",
  "cf-swift",
  "cf-swm",
  "cf-swt",
  "cf-swtc",
  "cf-sxdt",
  "cf-sys",
  "cf-taas",
  "cf-tau",
  "cf-tbar",
  "cf-tbx",
  "cf-tct",
  "cf-tdx",
  "cf-tel",
  "cf-ten",
  "cf-tes",
  "cf-tfd",
  "cf-thc",
  "cf-theta",
  "cf-tie",
  "cf-tig",
  "cf-time",
  "cf-tio",
  "cf-tips",
  "cf-tit",
  "cf-tix",
  "cf-tka",
  "cf-tkn",
  "cf-tkr",
  "cf-tks",
  "cf-tky",
  "cf-tmt",
  "cf-tnb",
  "cf-tnc",
  "cf-tns",
  "cf-tnt",
  "cf-tok",
  "cf-tokc",
  "cf-tomo",
  "cf-tpay",
  "cf-trac",
  "cf-trak",
  "cf-trc",
  "cf-trct",
  "cf-trig",
  "cf-trst",
  "cf-true",
  "cf-trump",
  "cf-trust",
  "cf-trx",
  "cf-tsl",
  "cf-ttt",
  "cf-turbo",
  "cf-tusd",
  "cf-tx",
  "cf-tzc",
  "cf-ubq",
  "cf-ubt",
  "cf-ubtc",
  "cf-ucash",
  "cf-ufo",
  "cf-ufr",
  "cf-ugc",
  "cf-uip",
  "cf-uis",
  "cf-ukg",
  "cf-unb",
  "cf-uni",
  "cf-unify",
  "cf-unit",
  "cf-unity",
  "cf-uno",
  "cf-up",
  "cf-uqc",
  "cf-usdt",
  "cf-usnbt",
  "cf-utc",
  "cf-utk",
  "cf-utnp",
  "cf-uuu",
  "cf-v",
  "cf-vee",
  "cf-vet",
  "cf-veri",
  "cf-via",
  "cf-vib",
  "cf-vibe",
  "cf-vit",
  "cf-viu",
  "cf-vivo",
  "cf-vme",
  "cf-voise",
  "cf-vrc",
  "cf-vrm",
  "cf-vrs",
  "cf-vsl",
  "cf-vsx",
  "cf-vtc",
  "cf-vtr",
  "cf-vzt",
  "cf-wabi",
  "cf-wan",
  "cf-wand",
  "cf-warp",
  "cf-waves",
  "cf-wax",
  "cf-wct",
  "cf-wdc",
  "cf-wgr",
  "cf-whl",
  "cf-wild",
  "cf-wings",
  "cf-wish",
  "cf-wpr",
  "cf-wrc",
  "cf-wtc",
  "cf-xas",
  "cf-xaur",
  "cf-xbl",
  "cf-xbp",
  "cf-xbts",
  "cf-xby",
  "cf-xcp",
  "cf-xcpo",
  "cf-xct",
  "cf-xcxt",
  "cf-xdce",
  "cf-xdn",
  "cf-xel",
  "cf-xem",
  "cf-xes",
  "cf-xgox",
  "cf-xhv",
  "cf-xin",
  "cf-xios",
  "cf-xjo",
  "cf-xlm",
  "cf-xlr",
  "cf-xmcc",
  "cf-xmg",
  "cf-xmo",
  "cf-xmr",
  "cf-xmx",
  "cf-xmy",
  "cf-xnk",
  "cf-xnn",
  "cf-xp",
  "cf-xpa",
  "cf-xpd",
  "cf-xpm",
  "cf-xptx",
  "cf-xrh",
  "cf-xrl",
  "cf-xrp",
  "cf-xsh",
  "cf-xsn",
  "cf-xspec",
  "cf-xst",
  "cf-xstc",
  "cf-xtl",
  "cf-xto",
  "cf-xtz",
  "cf-xvg",
  "cf-xyo",
  "cf-xzc",
  "cf-yee",
  "cf-yoc",
  "cf-yoyow",
  "cf-ytn",
  "cf-zap",
  "cf-zcl",
  "cf-zco",
  "cf-zec",
  "cf-zen",
  "cf-zeni",
  "cf-zeph",
  "cf-zer",
  "cf-zet",
  "cf-zil",
  "cf-zipt",
  "cf-zla",
  "cf-zny",
  "cf-zoi",
  "cf-zpt",
  "cf-zrx",
  "cf-zsc",
  "cf-zzc",
  "cf-aoa",
  "cf-apl",
  "cf-auto",
  "cf-bch",
  "cf-bsv",
  "cf-bz",
  "cf-cnus",
  "cf-ethos",
  "cf-eurs",
  "cf-gas",
  "cf-grin",
  "cf-gusd",
  "cf-hc",
  "cf-hyc",
  "cf-leax",
  "cf-mft",
  "cf-nex",
  "cf-noah",
  "cf-ors",
  "cf-pax",
  "cf-qnt",
  "cf-sc",
  "cf-snt",
  "cf-tosc",
  "cf-usdc",
  "cf-xpx",
  "cf-zb",
]
