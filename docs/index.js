import popper from 'popper.js'
import $ from 'jquery'
import bootstrap from 'bootstrap'
import 'bootstrap/scss/bootstrap.scss'

import icons from './icons';

(function () {
  const getIconsList = (filter) => {
    let availableIcons = icons;
    const itemTemplate = $('#icon-item-template').html().trim();
    const itemContainer = $('#coin-item-container').empty();

    const appendIconItem = (icon) => {
      const $item = $.parseHTML(itemTemplate.replace(/__ICON__/g, icon));
      itemContainer.append($item);
    }

    if (typeof filter !== typeof undefined && filter.length > 0) {
      const regexp = new RegExp(filter);
      availableIcons = icons.filter(icon => regexp.test(icon));
    }

    availableIcons.forEach(appendIconItem);
  }

  $(document).ready(() => {
    getIconsList();

    $(document).on('keyup', '#searchbar', function () {
      const filterStr = $(this).val().trim();
      getIconsList(filterStr);
    });

    $(document).on('submit', '#formSearchBar', function (e) {
      e.preventDefault();
      getIconsList($('#searchbar').val().trim());
      return false;
    });
  });
})()
